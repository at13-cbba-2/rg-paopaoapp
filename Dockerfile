FROM openjdk:11
WORKDIR /app
COPY ./thirdparty /app/thirdparty
COPY ./archive /app/archive
COPY ./build/libs/converter-0.0.1-SNAPSHOT.jar /converter-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/converter-0.0.1-SNAPSHOT.jar"]